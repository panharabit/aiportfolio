class ContactmesController < ApplicationController

  def index
    @category1 = Categoryskill.find(1)
    @skill1 = @category1.skills

    @category2 = Categoryskill.find(2)
    @skill2 = @category2.skills

    @category3 = Categoryskill.find(3)
    @skill3 = @category3.skills

    @categorystory = Categorystory.all

    @story = Story.all
    @personals = Personal.all
    @experience = Experience.all
    @education = Education.all
    @latestposts = Blog.limit(3).order("created_at ASC")
    @blog = Blog.all
    @contact = Contact.all

  end

  def new
    @category1 = Categoryskill.find(1)
    @skill1 = @category1.skills

    @category2 = Categoryskill.find(2)
    @skill2 = @category2.skills

    @category3 = Categoryskill.find(3)
    @skill3 = @category3.skills

    @categorystory = Categorystory.all

    @story = Story.all
    @personals = Personal.all
    @experience = Experience.all
    @education = Education.all
    @latestposts = Blog.limit(3).order("created_at ASC")
    @blog = Blog.all
    @contact = Contact.all
    @contactme = Contactme.new
  end

  def create
    @category1 = Categoryskill.find(1)
    @skill1 = @category1.skills

    @category2 = Categoryskill.find(2)
    @skill2 = @category2.skills

    @category3 = Categoryskill.find(3)
    @skill3 = @category3.skills

    @categorystory = Categorystory.all

    @story = Story.all
    @personals = Personal.all
    @experience = Experience.all
    @education = Education.all
    @latestposts = Blog.limit(3).order("created_at ASC")
    @blog = Blog.all
    @contact = Contact.all
    @contactme = Contactme.new(contactme_params)
    if @contactme.valid?
      NoticeMailer.sendmail_confirm(@contactme).deliver
      flash[:notice] = "Message sent"
      render :action => 'index'
    else
      render :action => 'new'
    end
  end

  private
    def contactme_params
      params.require(:contactme).permit(:name,:email,:subject,:message)
    end
end
