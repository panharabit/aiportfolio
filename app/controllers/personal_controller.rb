class PersonalController < ApplicationController

  def new
    @personal = Personal.new
  end

  def create
    @personal = Personal.new(personal_params)
    @personal.save
  end

  private
    def personal_params
      params.require(:personal).permit(:name,:position,:description,:dateofbirth,:email,:phone,:socialmedianame,:image)
    end
end
