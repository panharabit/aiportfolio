class StoriesController < ApplicationController

  def new
    @story = Story.new
  end
  def create
    @story = Story.new(story_params)
    @story.save
  end

  private
    def story_params
      params.require(:story).permit(:title,:description,:post_date,:image,:categorystory_id)
    end
end
