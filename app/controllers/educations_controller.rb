class EducationsController < ApplicationController

  def new
    @education = Education.new
  end

  def create
    @education = Education.new(edu_params)
  end

  private
    def edu_params
      params.require(:education).permit(:title,:start_education,:end_education,:description)
    end
end
