class SkillsController < ApplicationController
  def index
    @category = Categoryskill.find(params[:categoryskill_id])
    @skills = @category.skills
  end
end
