class BlogsController < ApplicationController

  def new
    @blog = Blog.new
  end
  def create
    @blog = Blog.new(blog_params)
  end
  def show
    @blog = Blog.find(params[:id])
    @personal = Personal.all
  end

  private
    def blog_params
      params.require(:blog).permit(:title,:description,:author,:image)
    end
end
