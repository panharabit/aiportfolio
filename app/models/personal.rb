class Personal < ApplicationRecord
  validates_presence_of :name
  validates_presence_of :position
  validates_presence_of :description
  validates_presence_of :dateofbirth
  validates_presence_of :address
  validates_presence_of :email
  validates_presence_of :phone
  validates_presence_of :socialmedianame

  has_attached_file :image, styles: {
    medium: "300x300>",
    thumb: "100x100>"
  }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
