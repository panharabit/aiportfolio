class Categoryskill < ApplicationRecord
  validates_presence_of :categoryname
  has_many :skills
end
