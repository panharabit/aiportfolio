class Skill < ApplicationRecord
  belongs_to :categoryskill
  validates_presence_of :skillname,:progressbar
  has_attached_file :image, styles: {
    medium: "300x300>",
    thumb: "100x100>"
  }
end
