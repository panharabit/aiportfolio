ActiveAdmin.register Blog do
  permit_params :title,:description,:author,:image

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
        f.input :title
        f.input :author
        f.input :description
        f.file_field :image, hint: f.blog.image? ? image_tag(blog.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
        actions
      end
    end

    show do |t|
      attributes_table do
        row :title
        row :author
        row :description do |d|
          raw d.description
        end
        row :image do
          blog.image? ? image_tag(blog.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

    index do
      selectable_column
      column link_to "ID" do |blog|
        link_to blog.id,admin_blog_path(blog)
      end
      column :title
      column :author
      column :description, :sortable => :description do |description|
        div :class=>"ass" do
          truncate(strip_tags(description.description))
        end
      end
      column :created_at
      column :updated_at
      column link_to "Image File" do |blog|
        image_tag(blog.image.url, height:'100')
      end
      actions
    end

end
