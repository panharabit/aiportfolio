ActiveAdmin.register Skill do
  permit_params :skillname,:progressbar,:categoryskill_id
  show :title => :skillname
  filter :skillname
  filter :categoryskill_id
  form do |f|
    f.inputs do
      f.collection_select :categoryskill_id,Categoryskill.all,:id,:categoryname
      f.input :skillname
      f.input :progressbar
    end
    f.actions
  end

  index do
    selectable_column
    column :id
    column :skillname
    column :progressbar
    column :categoryskill_id
    column :created_at
    column :updated_a

    actions
  end

end
