ActiveAdmin.register Experience do
  permit_params :title,:position,:startjobdate,:description
  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :title
      f.input :position
      f.input :startjobdate, as: :datepicker
      f.input :description
    end
    f.actions
  end

end
