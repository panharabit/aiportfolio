ActiveAdmin.register Categorystory do
  permit_params :categoryname
  show :title => :categoryname
  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :categoryname
      actions
    end
  end
end
