ActiveAdmin.register Education do
  permit_params :title,:start_education,:end_education,:description


  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :title
      f.input :start_education,as: :datepicker
      f.input :end_education,as: :datepicker
      f.input :description
      actions
    end
  end

end
