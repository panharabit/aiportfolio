ActiveAdmin.register Story do
  permit_params :title,:description,:post_date,:image,:categorystory_id
  filter :title

  form do |f|
    f.inputs do
      f.collection_select :categorystory_id,Categorystory.all,:id,:categoryname
      f.input :title
      f.input :description
      f.input :post_date, as: :datepicker
      f.file_field :image, hint: f.story.image? ? image_tag(story.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
    end
    f.actions
  end

    index do
      selectable_column
      column :id
      column :title
      column :description, :sortable => :description do |description|
          truncate(strip_tags(description.description))
      end
      column :post_date, as: :datepicker
      column link_to "Image File" do |story|
        image_tag(story.image.url, height:'100')
      end
      column :categorystory_id
      actions
    end

    show do |t|
      attributes_table do
        row :title
        row :description do |d|
          raw d.description
        end
        row :post_date, as: :datepicker
        row :image do
          story.image? ? image_tag(story.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

end
