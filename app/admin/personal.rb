ActiveAdmin.register Personal do
  permit_params :name,:position,:description,:dateofbirth,:address,:email,:phone,:socialmedianame,:image
  show do |t|
    attributes_table do
      row :name
      row :position
      row :description do |d|
        raw d.description
      end
      row :dateofbirth
      row :address
      row :email
      row :phone
      row :socialmedianame
      row :image do
        personal.image? ? image_tag(personal.image.url, height: '100') : content_tag(:span, "No photo yet")
      end
    end
  end

  filter :name

  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :name
      f.input :position
      f.input :description
      f.input :dateofbirth, as: :datepicker
      f.input :address
      f.input :email
      f.input :phone
      f.input :socialmedianame
      f.file_field :image, hint: f.personal.image? ? image_tag(personal.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
      actions
    end
  end

  index do
    selectable_column
    column link_to "ID" do |personal|
      link_to personal.id,admin_personal_path(personal)
    end
    column :name
    column :position
    column :description, :sortable => :description do |description|
      div :class=>"ass" do
        truncate(strip_tags(description.description))
      end
    end
    column :dateofbirth
    column :address
    column :email
    column :phone
    column :socialmedianame
    column :created_at
    column :updated_at
    column link_to "Image File" do |personal|
      image_tag(personal.image.url, height:'100')
    end
    actions
  end


end
