Rails.application.routes.draw do
  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :categoryskills do
    resources :skills
  end
  resources :categorystories do
    resources :stories
  end
  resources :contactmes, only: [:index,:new, :create]
  resources :homes
  root "homes#index"
end
