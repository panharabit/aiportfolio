class CreatePersonals < ActiveRecord::Migration[5.0]
  def change
    create_table :personals do |t|
      t.string :name
      t.string :position
      t.text :description
      t.date :dateofbirth
      t.string :address
      t.string :email
      t.string :phone
      t.string :socialmedianame
      t.string :image

      t.timestamps
    end
  end
end
