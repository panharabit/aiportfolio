class CreateSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :skills do |t|
      t.string :skillname
      t.integer :progressbar
      t.references :categoryskill, foreign_key: true , index: true

      t.timestamps
    end
  end
end
