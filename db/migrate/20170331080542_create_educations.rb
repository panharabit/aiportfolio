class CreateEducations < ActiveRecord::Migration[5.0]
  def change
    create_table :educations do |t|
      t.string :title
      t.date :start_education
      t.date :end_education
      t.text :description

      t.timestamps
    end
  end
end
