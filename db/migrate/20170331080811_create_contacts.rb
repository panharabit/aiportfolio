class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string :companyname
      t.string :address
      t.string :city
      t.string :email
      t.string :phone
      t.string :website

      t.timestamps
    end
  end
end
