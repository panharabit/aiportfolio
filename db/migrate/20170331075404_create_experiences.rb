class CreateExperiences < ActiveRecord::Migration[5.0]
  def change
    create_table :experiences do |t|
      t.string :title
      t.string :position
      t.date :startjobdate
      t.text :description

      t.timestamps
    end
  end
end
